function degToRad(deg) {
    return deg * Math.PI / 180
}
function radToDeg(rad) {
    return rad * 180 / Math.PI
}

const SCREEN_WIDTH = window.innerWidth
const SCREEN_HEIGHT = window.innerHeight

const canvas = document.createElement("canvas")
canvas.setAttribute("width", SCREEN_WIDTH)
canvas.setAttribute("height", SCREEN_HEIGHT-90)
context = canvas.getContext("2d")
const gd = document.createElement("div")
const text = document.createElement("p")
text.innerText = "HELLO"
gd.setAttribute("width", 45)
gd.setAttribute("height", 45)
gd.style.position = "abolute"
document.body.appendChild(gd)
gd.appendChild(text)
document.body.appendChild(canvas)


const PLAYER_SPEED = 8
 
const TICK = 30

const CELL_SIZE = 128

const PLAYER_SIZE = 8

const drawRays = true

const FOV = degToRad(60)

const COLORS = {
    floor: "#d52b1e", // "#ff6361"
    ceiling: "#ffffff", // "#012975",
    wall: "#013aa6", // "#58508d"
    wallDark: "#012975", // "#003f5c"
    rays: "#ffa600",
}

const map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  ];

const player = {
    x: CELL_SIZE * 1.5,
    y: CELL_SIZE * 2,
    veritcal_direction: 0,
    angle: degToRad(45),
    speed: 0
}
text.innerText = "This is a heading"


function clearScreen() {
    context.fillStyle = COLORS.sky
    context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)
}

function movePlayer() {
    newX = player.speed%2 == 0 ? player.x + Math.cos(player.angle) * player.speed : "s"
    cellX = Math.floor((newX-PLAYER_SIZE/2) / CELL_SIZE)
    newY = player.y + Math.sin(player.angle) * player.speed
    cellY = Math.floor((newY-PLAYER_SIZE/2) / CELL_SIZE)
    if(!map[cellY][cellX]){
        player.x = newX
        player.y = newY
    }
}
function outOfMapBounds(x, y) {
    return x < 0 || x >= map[0].length || y < 0 || y >= map.length
}

function distance(x, y, nX, nY) {
    return Math.sqrt(Math.pow(nY - y, 2) + Math.pow(nX - x, 2))
}


function getVCollision(angle) {
    const right = Math.abs(Math.floor((angle - Math.PI / 2) / Math.PI) % 2)
  
    const firstX = right
      ? Math.floor(player.x / CELL_SIZE) * CELL_SIZE + CELL_SIZE
      : Math.floor(player.x / CELL_SIZE) * CELL_SIZE
  
    const firstY = player.y + (firstX - player.x) * Math.tan(angle)
  
    const xA = right ? CELL_SIZE : -CELL_SIZE
    const yA = xA * Math.tan(angle)
  
    let wall
    let nextX = firstX
    let nextY = firstY
    while (!wall) {
      const cellX = right
        ? Math.floor(nextX / CELL_SIZE)
        : Math.floor(nextX / CELL_SIZE) - 1
      const cellY = Math.floor(nextY / CELL_SIZE)
  
      if (outOfMapBounds(cellX, cellY)) {
        break
      }
      wall = map[cellY][cellX]
      if (!wall) {
        nextX += xA
        nextY += yA
      } else {
      }
    }
    return {
      angle,
      distance: distance(player.x, player.y, nextX, nextY),
      vertical: true,
    }
  }
  
function getHCollision(angle) {
    const up = Math.abs(Math.floor(angle / Math.PI) % 2)
    const firstY = up
      ? Math.floor(player.y / CELL_SIZE) * CELL_SIZE
      : Math.floor(player.y / CELL_SIZE) * CELL_SIZE + CELL_SIZE
    const firstX = player.x + (firstY - player.y) / Math.tan(angle)
  
    const yA = up ? -CELL_SIZE : CELL_SIZE
    const xA = yA / Math.tan(angle)
  
    let wall
    let nextX = firstX
    let nextY = firstY
    while (!wall) {
      const cellX = Math.floor(nextX / CELL_SIZE)
      const cellY = up
        ? Math.floor(nextY / CELL_SIZE) - 1
        : Math.floor(nextY / CELL_SIZE)
  
      if (outOfMapBounds(cellX, cellY)) {
        break
      }
  
      wall = map[cellY][cellX]
      if (!wall) {
        nextX += xA
        nextY += yA
      }
    }
    return {
      angle,
      distance: distance(player.x, player.y, nextX, nextY),
      vertical: false,
    }
  }
  
function castRay(angle) {
    const vCollision = getVCollision(angle)
    const hCollision = getHCollision(angle)

    return hCollision.distance >= vCollision.distance ? vCollision : hCollision
}

function getRays() {
    const initialAngle = player.angle - FOV / 2
    const numberOfRays = SCREEN_WIDTH
    const angleStep = FOV / numberOfRays
    return Array.from({ length: numberOfRays }, (_, i) => {
        const angle = initialAngle + i * angleStep
        const ray = castRay(angle)
        return ray
    })
}
function fixFishEye(distance, angle, playerAngle){
    let diff = angle-playerAngle
    return distance*Math.cos(diff)
}
function renderScene(rays) { 
    rays.forEach((ray, i) => {
        const distance = fixFishEye(ray.distance, ray.angle, player.angle)
        const wallHeight = ((CELL_SIZE*5) / distance) * 256
        context.fillStyle = ray.vertical ? COLORS.wallDark : COLORS.wall
        context.fillRect(i , SCREEN_HEIGHT / 2 - wallHeight /2, 1, wallHeight)
        context.fillStyle = COLORS.floor
        context.fillRect(i, SCREEN_HEIGHT/2 + wallHeight /2, 1, SCREEN_HEIGHT/2 - wallHeight /2)

        //draw crosshair
        
    })
}
function renderMinimap(posX, posY, scale, rays) {
    const cellSize = scale * (CELL_SIZE-1)
    //draw map
    map.forEach((row, y) => {
        row.forEach((cell, x) => {
            if (cell) {
                context.fillStyle = "grey"
                context.fillRect(posX + x * cellSize, posY + y * cellSize, cellSize, cellSize)
            }
        })
    })

    // draw player
    context.fillStyle = "black"
    context.fillRect(
        posX + player.x * scale - PLAYER_SIZE / 2,
        posY + player.y * scale - PLAYER_SIZE / 2,
        PLAYER_SIZE,
        PLAYER_SIZE
    )

    //direction
    raylength = PLAYER_SIZE * 2
    context.strokeStyle = "red"
    context.beginPath()
    context.moveTo(
        player.x * scale + posX,
        player.y * scale + posY
    )
    context.lineTo(
        (player.x + Math.cos(player.angle) * raylength) * scale,
        (player.y + Math.sin(player.angle) * raylength) * scale
    )
    context.closePath()
    context.stroke()
    if (drawRays){
        // render rays
        context.strokeStyle = COLORS.rays
        rays.forEach(ray => {
            context.beginPath()
            context.moveTo(
                player.x * scale + posX,
                player.y * scale + posY
                )
                context.lineTo(
                    (player.x + Math.cos(ray.angle) * ray.distance) * scale,
                    (player.y + Math.sin(ray.angle) * ray.distance) * scale
        )
        context.closePath()
        context.stroke()
        })
    }
    
    
}


function gameLoop() {
    clearScreen()
    movePlayer()
    const rays = getRays()
    renderScene(rays)
    renderMinimap(0, 0, 0.25, rays)
}

document.addEventListener("keydown", (e) => {
    if (e.key == "ArrowUp" || e.key == "w") {
        player.speed = PLAYER_SPEED
    }
    if (e.key == "ArrowDown" || e.key == "s") {
        player.speed = -PLAYER_SPEED
    }
})

document.addEventListener("keyup", (e) => {
    if (e.key == "ArrowUp" || e.key == "ArrowDown" || e.key == "w" || e.key == "s") {
        player.speed = 0
    }
})

document.addEventListener("mousemove", (e) => {

    player.angle += degToRad(e.movementX)
    player.veritcal_direction = e.screenY
})
setInterval(gameLoop, TICK)