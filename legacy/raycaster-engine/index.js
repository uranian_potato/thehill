function degToRad(deg) {
    return deg * Math.PI / 180
}
function radToDeg(rad) {
    return rad * 180 / Math.PI
}

class Game {
    constructor(map, SCREEN_WIDTH, SCREEN_HEIGHT, TICK, FOV, COLORS, CELL_SIZE, PLAYER_SIZE, PLAYER_SPEED, drawRays) {
        this.map = map

        this.SCREEN_WIDTH = SCREEN_WIDTH || window.innerWidth
        this.SCREEN_HEIGHT = SCREEN_HEIGHT || window.innerHeight

        const canvas = document.createElement("canvas")
        canvas.setAttribute("width", SCREEN_WIDTH)
        canvas.setAttribute("height", SCREEN_HEIGHT-90)
        this.context = canvas.getContext("2d")
        this.gd = document.createElement("div")
        this.text = document.createElement("p")
        this.gd.setAttribute("width", 45)
        this.gd.setAttribute("height", 45)
        this.gd.style.position = "absolute"
        document.body.appendChild(gd)
        gd.appendChild(text)
        document.body.appendChild(canvas)
        
        this.PLAYER_SPEED = PLAYER_SPEED || 8
        
        this.TICK = TICK || 30

        this.CELL_SIZE = CELL_SIZE || 128

        this.PLAYER_SIZE = PLAYER_SIZE || 8

        this.drawRays = drawRays || false

        this.FOV = FOV || degToRad(60)
        this.COLORS = COLORS || {
            floor: "#d52b1e", // "#ff6361"
            ceiling: "#ffffff", // "#012975",
            wall: "#013aa6", // "#58508d"
            wallDark: "#012975", // "#003f5c"
            rays: "#ffa600",
        }
        
    }
}