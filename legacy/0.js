import * as THREE from './three.js/src/Three.js'
import {GLTFLoader} from './three.js/examples/jsm/loaders/GLTFLoader.js'

function LoadingBar(display="none", width=0){
    try{
        let loaded  = document.querySelector('#loaded')
        loaded.setAttribute("width", width)
        loaded.style.display = display
    }catch{
        const loadingbar = document.createElement("div")
        loadingbar.setAttribute("width", 100)
        loadingbar.setAttribute("height", 10)
        loadingbar.style.position = "absolute"
        loadingbar.style.backgroundColor = '#ffff'
        loadingbar.style.border = '10px solid'
        loadingbar.style.top = window.innerHeight/2
        loadingbar.style.left = window.innerWidth/2
        loadingbar.style.display = display
        const loaded = document.createElement("div")
        loaded.setAttribute("id", "loaded")
        loaded.setAttribute("width", width)
        loaded.setAttribute("height", 5)
        loaded.style.position = "absolute"
        loaded.style.top = window.innerHeight/2
        loaded.style.left = window.innerWidth/2
        loaded.style.display = display
        loaded.style.backgroundColor = "#0001"
        document.body.appendChild(loaded)
        document.body.appendChild(loadingbar)
    }
}


let gameObjects = []
let gameMeshes = []
let objCount = 1
class Game {
    constructor(scene, camera){
        this.SCREEN_WIDTH = window.innerWidth
        this.SCREEN_HEIGHT = window.innerHeight
        this.loadingbardisplay = "none"
        const canvas = document.querySelector("#c")
        this.renderer = new THREE.WebGLRenderer({canvas})
        document.body.appendChild(canvas)
        this.scene = scene
        this.camera = camera
    }
    render(){
        try{
            this.renderer.render(this.scene, this.camera)
            return true
        }
        catch (err){
            console.log("000")
            console.error(err)
            return false
        }
    }
}

class GameObject{
    /**
     * Materials
     * Transform positions
    */
    constructor(name, debug=false) {
        this.name = name || objCount
        objCount++
        this.debug = debug
        this.presetDir = {"cube": './resources/presets/cube.gltf', "scene": "./resources/presets/scene.gltf"}
        gameObjects.push(this)
    }
    update(){

    }
    load(loadPath=undefined, preset=undefined){
        let loader = new GLTFLoader()
        if(preset){
            loader.load(this.presetDir[preset], this.onLoad, this.onProgress, this.onError)
        }
        else {
            loader.load(loadPath, this.onLoad, this.onLoad, this.onProgress, this.onError)
        }
    }

    onLoad(object){
        LoadingBar()
        console.log(object)
        object = true
        let obj = new THREE.Object3D()
        obj.add(object)
        gameMeshes.push(obj)
        console.log("object " + objCount + " has been fully loaded")
        objCount++
    }
    onProgress(xhr){
        let percentage = Math.floor((xhr.loaded/xhr.total) * 100)
        LoadingBar("block", percentage)
    }
    onError(error){
        console.log("001")
        if (this.debug ){
            console.error(error)
        }
    }

    setUpdate(func){
        this.update = func
        let pos = gameObjects.indexOf(this)
        delete gameObjects[pos]
        gameObjects[pos] = this
    }
    setTransform(transform=new THREE.Vector3(0,0,0)){
        this.obj.position = transform
    }
}

class Camera extends GameObject {
    constructor(name, debug, type='PC') {
        super(name, debug)
        this.camTypes = {'PC': THREE.PerspectiveCamera}
        this.cam = new this.camTypes[type]()
        this.cam.position.z = -2
    }
    setCamTransform(transform=new THREE.Vector3(0,0,0)){
        this.cam.position = transform
    }
}

class Scene extends GameObject {
    constructor(name, debug) {
        super(name, debug)
        this.scene = new THREE.Scene()
    }
    add(){
        for(let mesh of gameMeshes){
            if(!this.scene.children.includes(mesh)){
                this.scene.add(mesh)
            }
        }
    }
    update(){
        this.add()
    }
}
class Box  extends GameObject{
    constructor(name, debug, proportions=new THREE.Vector3(0,0,0), transform=new THREE.Vector3(0,0,0), matPreset, matObject) {
        let materials = {'MBM': THREE.MeshBasicMaterial, 'MPM': THREE.MeshPhongMaterial}
        super(name, debug)
        this.material = new materials[matPreset](matObject)
        this.geometry = new THREE.BoxGeometry(proportions.x, proportions.y, proportions.z)
        this.mesh = new THREE.Mesh(this.geometry, this.material)
        this.mesh.position.x = transform.x
        this.mesh.position.y = transform.y
        this.mesh.position.z = transform.z

        gameMeshes.push(this.mesh)
    }

}
export {Game, GameObject, Camera, Scene, Box, gameMeshes, gameObjects, THREE}