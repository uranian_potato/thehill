import * as Kengine1 from './kengine1.js'
const TICK = 30
function main(){
    const canvas = document.createElement("canvas")
    canvas.setAttribute("id", "c")
    canvas.setAttribute("width", window.innerWidth - 90)
    canvas.setAttribute("height", window.innerHeight - 90)
    document.body.appendChild(canvas)

    var scene0 = new Kengine1.Scene("scene0", false)
    var cam0 = new Kengine1.Camera("cam0", false)
    var game = new Kengine1.Game(scene0.scene, cam0.cam)
    var cube = new Kengine1.Box(
        "cube",
        true, 
        new Kengine1.THREE.Vector3(1,1,1), 
        new Kengine1.THREE.Vector3(0,0,1), 
        'MPM', 
        {}
    )

    scene0.add()
    requestAnimationFrame(updateLoop) // called per refresh
    //setInterval(fixedupdateLoop, TICK) // called every TICK milliseconds

    function updateLoop(){
        for(let obj of Kengine1.gameObjects){
            obj.update()
        }
        game.render()
        requestAnimationFrame(updateLoop)
    }
    function fixedupdateLoop(){
        game.render()
    }
}
main()
