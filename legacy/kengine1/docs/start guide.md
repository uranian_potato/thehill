# Quick START


First clone the repo using git or download the zip file
```
git clone https://uranian_potato@bitbucket.org/uranian_potato/thehill.git
```
then create two new files 'index.html' and 'main.js' into the project folder.

the file hirearchy should look like this:
```
- >theHill
    |- >legacy 
    |- >resources
    |- kengine1.js
    create new:
    |- main.js
    |- index.html
```
---
in the index.html add this boilerplate code:
```
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>theHill</title>
		<style>
			body { margin: 0; }
		</style>
	</head>
	<body>
        <script type="module" src="./main.js"></script>
	</body>
</html>
```
this imports main js file as a 'module' allowing it to use the `import` statement.

---

To initialize the game now go to the main.js file and  import `theHill` from 
the file kengine1.js into your main.js file

```
import {theHill} from './kengine1.js'
```

then create a new instance of `theHill` and call the default `.run()` method.

```
let gameInstance = new theHill()
gameInstance.run()
```
___

To acually run the game either us the live server extension in vs code to launch the index.html file or u can use the npm module `live-server` from the root directory like so:
```
> cd path\to\theHill
> npx live-server
```
> be aware that you will probably need to use Visual Studio Code's live server extension to properly run the game

