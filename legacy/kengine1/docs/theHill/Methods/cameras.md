# Camera methods

## perspectiveCameraSetup

this method sets up a basic perspective camera
inputs:
(fov, aspect, near, far, position)

fov: field of view, number between 0 and 180

aspect: aspect ratio, width divided by height

near, far: clipping planes, the deges of the cameras sight

positon : Vec3 detailing the position of the camera