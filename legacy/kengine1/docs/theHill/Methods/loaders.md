# Loader methods

## loadStaticGLTF

this method loads static gltf format 3d objects into the scene, it takes in a file path as an input.

## loadStaticFBX

this method loads static fbx format 3d objects into the scene, it takes in a file path as an input.

## loadAnimatedFBX

this method loads one static fbx format 3d object and an array of all its animations into the scene, it takes the type, name, path to model, and array of paths to animations as input