# Core methods

## initialze
this method intializes the subclasses and adds the relevent ones as properties, it is executed in the constructor

## run
this method is defaulted to setup a basic scene

you can override it to do as you wish like so:

 in main.js:
```
import {theHill} from './kengine1.js'
gameInstance = new theHill()
gameInstance.run = () => {
    //if you want to access other methods in the class:
    //gameInstance.{add class here}
    //example:

    gameInstance.initRenderer()
} 
```
```
// refrain from importing threejs directly to prevent multiple imports.
// instead import it from kengine1.js
import {theHill, THREE} from './kengine1.js'

gameInstance = new theHill()
gameInstance.run = () => {
    let scene = new  THREE.Scene()
}
```
> please refrain from using threejs directly in main.js as much as possible to keep code organized


call `run` at the end to 'run' the game
```
gameInstacne.run()
```


## initRenderer

this function initialises `THREE.WebGLRenderer` with all the settings required and stores it in the object attribute `this._threejs`

## OnWindowResize

this method is a callback which is executed every time that the window is resized to adjust the camera settings.

## RAF

this method stands for "request animation frame"  and is a callback which is executed everytime the browser redraws the display. The frequency depends on the browser its running in and the capabilities of the harware. 

you can override it to add more functions that need to be executed. It functions as the recursive game loop.

## IC 
this is similar to RAF, but it instead is called every `this.TICK` interval, and doesnt depend on anything but time.