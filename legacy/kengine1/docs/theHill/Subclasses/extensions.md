# Extended Subclasses

## CharacterFSM
 Finite state machine that is initialized with Character States states

## IdleState
Idle state for character FSM

## FroWalkState
Forward Walking state for FSM

## backWalkState
Backward Walking state for FSM
