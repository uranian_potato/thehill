# Base Sublasses

## CharacterController
basic character controller for the game player

## Finite State Machine
basic FSM for animations, it is initalized without any states

## keyboardInput
basic keyboard input poll. checks for WASD, space, and shift keys

## State
basic State class that is passed into an FSM