# Values

## _threejs

object of `THREE.WebGLRenderer`, used to render scene

## _scene

object of `THREE.Scene`, scene of the game

## _camera

Camera object 

## _entities

Object containing all the entities in the game instance.
The three atribbs are:
- objects : game objects 
- players : players
- npc : non-player characters