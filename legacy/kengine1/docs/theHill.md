# theHill

this is a class in the kengine1.js file. it is the object which makes up the game.
it consists of the following:
## Componenets

### Function
- getRandString

## Methods
 ### core
 - initialize
 - run
 - initRenderer
 - OnWindowResize
 - RAF
 - IC
 - Step
 ### loaders
 - loadStaticGLTF
 - loadStaticFBX
 - loadAnimatedFBX
 ### cameras
 - perspectiveCameraSetup
 ### lights
 - directionalLightSetup
 - ambientLightSetup
 ### controls
 - orbitControlsSetup
 ### enviroment
 -  skyboxSetup
 ###

## Values
- _threejs
- _scene
- _camera
- _entities
- _id
- _debug

## Variables
- TICK
- objCOunt
- _debug

## Subclasses
### base
- CharacterController
- FiniteStateMachine
- keyboardInput
- State
### extensions
- CharacterFSM
- IdleState
- FroWalkState
- BalkWalkState