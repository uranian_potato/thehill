import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.118/build/three.module.js'

import {FBXLoader} from 'https://cdn.jsdelivr.net/npm/three@0.118.1/examples/jsm/loaders/FBXLoader.js'
import { GLTFLoader } from 'https://cdn.jsdelivr.net/npm/three@0.118.1/examples/jsm/loaders/GLTFLoader.js'
import { OrbitControls } from 'https://cdn.jsdelivr.net/npm/three@0.118/examples/jsm/controls/OrbitControls.js'

function getRandString(length=16){
    //'1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'
    let key = '1234567890abcdef'
    let l = key.length
    let ind = length
    let outStr = ''
    while(ind>0){
        rand = Math.floor(Math.random() * l)
        console.log(rand)
        outStr += key[rand]
        ind -= 1
    }
    return outStr
}
class theHill {
    constructor(id=getRandString() ,debug = false, tick = 30) {
        this._id = id 
        this._debug = debug
        this.TICK = tick
        window.addEventListener('resize', () => {
            this.OnWindowResize()
        }, false)
        this._mixers = []
        this.objCount = 0
        this._entities = {
            "player": {},
            "objects": {},
            "npcs": {}
        }
        this.initialze()
    }
    run() {
        this.initRenderer()

        this._scene = new THREE.Scene()

        this.perspectiveCameraSetup()
        this.orbitControlsSetup()

        this.directionalLightSetup()
        this.ambientLightSetup(0xFFFFFF)
        this.skyboxSetup()
        this.planeSetup()
        this.loadAnimatedFBX('player', undefined, undefined, {idle:'./resources/tbr/Breathing Idle.fbx', froWalk:'./resources/tbr/Walk.fbx', backWalk: './resources/tbr/Walking Backwards.fbx'})
        this.RAF()
        setInterval(this.IC, this.TICK)
    }
    initRenderer() {
        this._threejs = new THREE.WebGLRenderer({
            antialias: true,
        })
        this._threejs.shadowMap.enabled = true
        this._threejs.shadowMap.type = THREE.PCFSoftShadowMap
        this._threejs.setPixelRatio(window.devicePixelRatio)
        this._threejs.setSize(window.innerWidth, window.innerHeight)
        document.body.appendChild(this._threejs.domElement)
    }
    perspectiveCameraSetup(fov = 60, aspect = window.innerWidth / window.innerHeight, near = 0.1, far = 2000, position = new THREE.Vector3(0, 0 , 0)) {
        //{x: 25, y: 47, z: -44}
        // class Camera{
        //     constructor(fov, aspect, near, far, positon) {
                
        //     }   
        // }
        let posCam = new THREE.Vector3(25+position.x, 47+position.y , -44+position.z)
        let cam = new THREE.PerspectiveCamera(fov, aspect, near, far)
        cam.position.set(posCam.x, posCam.y, posCam.z)
        this._camera = (cam)
    }
    directionalLightSetup() {
        let light = new THREE.DirectionalLight(0xFFFFFF, 1.0)
        light.position.set(20, 100, 10)
        light.target.position.set(0, 0, 0)
        light.castShadow = true
        light.shadow.bias = -0.001
        light.shadow.mapSize.width = 2048
        light.shadow.mapSize.height = 2048
        light.shadow.camera.near = 0.1
        light.shadow.camera.far = 500.0
        light.shadow.camera.near = 0.5
        light.shadow.camera.far = 500.0
        light.shadow.camera.left = 100
        light.shadow.camera.right = -100
        light.shadow.camera.top = 100
        light.shadow.camera.bottom = -100
        this._scene.add(light)
    }
    ambientLightSetup(color = 0x101010) {
        let light = new THREE.AmbientLight(color)
        this._scene.add(light)
    }
    orbitControlsSetup(pos = new THREE.Vector3(0, 20, 0)) {
        const controls = new OrbitControls(this._camera, this._threejs.domElement)
        controls.target.set(pos.x, pos.y, pos.z)
        controls.update()
    }
    skyboxSetup(index = '01') {
        const loader = new THREE.CubeTextureLoader()
        const texture = loader.load([
            'resources/skybox/'+index+'/posx.jpg',
            'resources/skybox/'+index+'/negx.jpg',
            'resources/skybox/'+index+'/posy.jpg',
            'resources/skybox/'+index+'/negy.jpg',
            'resources/skybox/'+index+'/posz.jpg',
            'resources/skybox/'+index+'/negz.jpg',
        ])
        this._scene.background = texture
    }
    planeSetup() {
        const plane = new THREE.Mesh(
            new THREE.PlaneGeometry(100, 100, 10, 10),
            new THREE.MeshStandardMaterial({
                color: 0xFFFFFF,
            }))
        plane.castShadow = false
        plane.receiveShadow = true
        plane.rotation.x = -Math.PI / 2
        this._scene.add(plane)
    }
    loadStaticGLTF(path = './resources/presets/scene.gltf') {
        try {
            let loader = new GLTFLoader()
            console.log(path)
            loader.load(path, (gltf) => {
                gltf.scene.traverse(c => {
                    c.castShadow = true
                })
                this._scene.add(gltf.scene)
            })
        }
        catch (err) {
            console.log("error loading: " + path + " into the scene")
            if (this._debug) {
                console.log(err)
            }
        }
    }
    loadStaticFBX(path='./resources/xbot/xbot.fbx'){
        try{
            let loader = new FBXLoader()
            loader.load(path, (fbx) => {
                fbx.scale.setScalar(0.1)
                fbx.traverse(c => {
                  c.castShadow = true
                })    
                this._scene.add(fbx)
            })
        }
        catch (err) {
            console.log("error loading: " + path + " into the scene")
            if (this._debug) {
                console.log(err)
            }
        }
    }
    loadAnimatedFBX(type='object', name=''+this.objCount,modelPath='./resources/tbr/Ch36_nonPBR.fbx', animPath={"idle" : './resources/zombieDemo/dance.fbx'}){
        try {
            let loader = new FBXLoader()
            loader.load(modelPath, (fbx) => {
                fbx.scale.setScalar(0.1)
                fbx.traverse(c => {
                    c.castShadow = true
                })          
                let animClips = {}
                try{
                    let completedLoops = 0
                    for(let animName in animPath){
                        let anim = new FBXLoader()
                        anim.load(animPath[animName], (anim) => {
                            let mixer = new THREE.AnimationMixer(fbx)
                            this._mixers.push(mixer)
                            let animation = mixer.clipAction(anim.animations[0])
                            animClips[animName] = animation
                            completedLoops++
                            try{
                                afterAnimLoad(this)
                            }catch(err){
                                console.log(err)
                                if(completedLoops >= animPath.length){
                                    console.error(err)
                                }
                            }
                        })
                    }
                }
                catch(err){
                    console.log("error loading the animations for: "+ modelPath )
                    if (this._debug) {
                        console.log(err)
                    }
                }
                function afterAnimLoad(mega){
                    if(mega._entities[type]){
                        mega._entities[type] = {"obj": fbx, "animClips": animClips}
                        mega._scene.add(fbx)
                        if(type == 'player'){
                            mega.controller = new mega.CharController(false, mega, mega.objCount)
                            
                        } else if(type == 'objects'){
    
                        }
                        else {
                            throw new Error(type+" type is not a game entity")
                        }
                        mega.objCount++
                    }
                }
            })
        }
        catch(err){
            console.log("error loading the animated model: " + modelPath + " into the scene")
            if (this._debug) {
                console.log(err)
            }
        }
    }
    OnWindowResize() {
        try {
            this._camera.aspect = window.innerWidth / window.innerHeight
            this._camera.updateProjectionMatrix()
            this._threejs.setSize(window.innerWidth, window.innerHeight)
        }
        catch (err) {
            if (this._debug) {
                console.log("window resize fail")
                console.log(err)
            }
        }
    }
    RAF() {
        requestAnimationFrame((t) => {
            if (this._previousRAF === null) {
                this._previousRAF = t
            }
            this.RAF()
            this._threejs.render(this._scene, this._camera)
            this.Step(t - this._previousRAF)
            this._previousRAF = t
            if(this.controller){
                this.controller.Update(t)
            }
        })
    }
    Step(timeElapsed) {
        const timeElapsedS = timeElapsed * 0.001
        if (this._mixers) {
          this._mixers.map(m => m.update(timeElapsedS))
        }
    }
    IC() {
    }
    initialze(){
        // class FSMProxy{
        //     constructor(animations = {}){
        //         this._animations = animations
        //     }
        //     get animations(){
        //         return this._animations
        //     }
        // }
        class CharacterController{
            constructor(ultDebug=false, instance = new theHill(), index=0, inputMethod='keyboard'){
                // try{
                    this.instance = instance
                    this.inputers = {'keyboard': keyboardInput}
                    this.input = new this.inputers[inputMethod]()
                    this._target = instance._entities.player.obj

                    this.speed = 0.6
                    this._stateMachine = new CharacterFSM(instance._entities.player)
                    this.debug = ultDebug
                // }
                // catch(err){
                //     console.log(err)
                // }
            }
            Update(timeElapsed){
                try{
                    if (!this._target) {
                        return
                    }
                    this._stateMachine.Update(timeElapsed, this.input)
                    if(this.input._keys.forward || this.input._keys.backward || this.input._keys.left || this.input._keys.right){
                        //determine the direction
                        let directionX = (this.input._keys.left ? this.speed : 0) + (this.input._keys.right ? -this.speed : 0)
                        let directionZ = (this.input._keys.backward ? -this.speed : 0) + (this.input._keys.forward ? this.speed : 0)
                        this.instance._entities.player.obj.position.z += directionZ
                        this.instance._entities.player.obj.position.x += directionX
                        this.instance._camera.position.x += directionX
                        this.instance._camera.position.z += directionZ
                    }
                }
                catch(err){
                    if(this.debug){
                        console.log(err)
                    }
                }
            }
        }

        class FiniteStateMachine {
            constructor(){
                this._states = {}
                this._currentState = null
            }
            AddState(name, type){
                this._states[name] = type
            }
            SetState(name){  
                const prevState = this._currentState
                if (prevState) {
                    if (prevState.Name == name) {
                      return
                    }
                    prevState.Exit()
                  }
                const state = new this._states[name](this)
                this._currentState = state
                state.Enter(prevState)
            }

            Update(timeElapsed, input) {
                if (this._currentState) {
                  this._currentState.Update(timeElapsed, input)
                }
            }
        }
        class CharacterFSM extends FiniteStateMachine{
            constructor(proxy) {
                super()
                this._states = {}
                this._proxy = proxy.animClips
                this.Init()
                let idleState = new this._states['idle'](this)
                this._currentState = idleState
                idleState.Enter()
            }
            Init(){
                this.AddState('idle', IdleState)
                this.AddState('froWalk', FroWalkState)
                this.AddState('backWalk', BackWalkState)
            }
        }
        
        class keyboardInput{
            constructor() {
                this.Init()
            }
            Init(){
                this._keys = {
                    forward: false,
                    backward: false,
                    left: false,
                    right: false,
                    space: false,
                    shift: false,
                  }
                  document.addEventListener('keydown', (e) => this.onKeyDown(e), false)
                  document.addEventListener('keyup', (e) => this.onKeyUp(e), false)
            }
            onKeyDown(e){
                switch (e.keyCode) {
                    case 87: // w
                    this._keys.forward = true
                    break
                    case 65: // a
                    this._keys.left = true
                    break
                    case 83: // s
                    this._keys.backward = true
                    break
                    case 68: // d
                    this._keys.right = true
                      break
                    case 32: // SPACE
                      this._keys.space = true
                      break
                    case 16: // SHIFT
                      this._keys.shift = true
                      break
                }
            }
            onKeyUp(e){
                switch (e.keyCode) {
                    case 87: // w
                      this._keys.forward = false
                      break
                    case 65: // a
                      this._keys.left = false
                      break
                    case 83: // s
                      this._keys.backward = false
                      break
                    case 68: // d
                      this._keys.right = false
                      break
                    case 32: // SPACE
                      this._keys.space = false
                      break
                    case 16: // SHIFT
                      this._keys.shift = false
                      break
                }
            }
        }
        
        class State {
            constructor(parent) {
            }
          
            Enter() {}
            Exit() {}
            Update() {}
        }
        
        class IdleState extends State {
            constructor(parent = new CharacterFSM()){
                super()
                this._parent = parent
            }
            get name() {
                return 'idle'
            }
            Enter(prevState){
                const idleAction = this._parent._proxy.idle
                if(prevState){
                    let prevAction = this._parent._proxy[prevState.name]
                    idleAction.time = 0.0
                    idleAction.enabled = true
                    idleAction.setEffectiveTimeScale(1.0)
                    idleAction.setEffectiveWeight(1.0)
                    idleAction.crossFadeFrom(prevAction, 0.5, true)
                    idleAction.play()
                }
                else{
                    idleAction.play()
                }
                return true
            }
            Update(_, input) {
                if (input._keys.forward) {
                    this._parent.SetState('froWalk')
                }
                else if(input._keys.backward){
                    this._parent.SetState('backWalk')
                }
            }
        }
        class FroWalkState extends State {
            constructor(parent = new CharacterFSM()){
                super()
                this._parent = parent
            }
            get name(){
                return 'froWalk'
            }
            Enter(prevState){
                const walkingAction = this._parent._proxy.froWalk
                if(prevState){
                    const prevAction = this._parent._proxy[prevState.name]
                    walkingAction.enabled = true
                    walkingAction.time = 0.0
                    walkingAction.setEffectiveTimeScale(1.0)
                    walkingAction.setEffectiveWeight(1.0)
                
                    walkingAction.crossFadeFrom(prevAction, 0.5, true)
                    walkingAction.play()
                } else {
                    walkingAction.play()
                }

                // set Velocity of Char COntroller
            }
            Update(_, input){
                if(!(input._keys.forward)) {
                    this._parent.SetState('idle')
                }
                else if(input._keys.backward && !(input._keys.forward)){
                    this._parent.SetState('backWalk')
                }
            }
        }
        class BackWalkState extends State {
            constructor(parent = new CharacterFSM()){
                super()
                this._parent = parent
            }
            get name(){
                return 'backWalk'
            }
            Enter(prevState){
                const walkingAction = this._parent._proxy.backWalk
                if(prevState){
                    const prevAction = this._parent._proxy[prevState.name]
                    walkingAction.enabled = true
                    walkingAction.time = 0.0
                    walkingAction.setEffectiveTimeScale(1.0)
                    walkingAction.setEffectiveWeight(1.0)
                
                    walkingAction.crossFadeFrom(prevAction, 0.5, true)
                    walkingAction.play()
                } else {
                    walkingAction.play()
                }

                // set Velocity of Char COntroller
            }
            Update(_, input){
                if(!(input._keys.backward)) {
                    this._parent.SetState('idle')
                }
                else if(input._keys.forward && !(input._keys.backward)){
                    this._parent.SetState('froWalk')
                }
            }
        }

        this.CharController = CharacterController
        //this.FSMProxy = FSMProxy
        this.FiniteStateMachine = FiniteStateMachine
        this.keyboardInput = keyboardInput
    }
}
export { theHill, THREE }