import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.118/build/three.module.js'
import { Lights } from './lights.js'
import { InputCollecter } from './input.js'
import { components } from './components.js'
import { gui } from './ui.js'
import { FBX } from './loaders.js'
import {CharacterController} from './characterController.js'
import {Line} from './debug.js'
import { ThirdPersonCamera } from './cameras.js'
class GAME{
    constructor(debug=false, id=undefined, tick=30, inputType='st'){
        this.controller = undefined

        this.initSubs(inputType)
        this.prevRAF = 0
        this.id = id || this.comps.getRandString(16)
        window.addEventListener('resize', () => {
            this.OnWindowResize()
        }, false)
        this.debug = debug

        this.entityCount = 0
        this.entities = {
            "player": {"name":undefined, "obj":undefined, "animClips":[]},
            "staticObjs":[],
            "dynamicObjs":[],
            "npc":[],
            "mods":[]
        }
        this.mixers = []

    }
    run(){
        this.initRenderer()

        this.scene = new THREE.Scene()
        new Line(THREE, this.scene)
        this.initLoaders()

        this.lights.ambientLightSetup()
        this.lights.directionalLightSetup()

        this.thirdPersonCamSetup()
        this.gui.addXandY()
        this.gui.addCSS('*', {"cursor": "none"})
        this.skyboxSetup()
        // this.FBX.loadStaticFBX('../resources/xbot/xbot.fbx', 'staticObjs')
        this.FBX.loadAnimatedFBX(
            undefined, undefined,
            '../resources/xbot/xbot.fbx',
            {
                'idle': '../resources/xbot/Standing Idle.fbx',
                'froWalk': '../resources/xbot/Walking.fbx',
                'backWalk': '../resources/xbot/Walking Backwards.fbx',
                //'leftStrafe': '../resources/tbr/Left Strafe Walking.fbx'
            },
            ThirdPersonCamera
        )

        const plane = new THREE.Mesh(
            new THREE.PlaneGeometry(100, 100, 10, 10),
            new THREE.MeshStandardMaterial({
                color: 0xFFFFFF,
            }))
        plane.castShadow = false
        plane.receiveShadow = true
        plane.rotation.x = -Math.PI / 2
        this.scene.add(plane)
        // this.gui.addBasicP('screen', 20, 400)

        this.RAF()
    }
    initRenderer(){
        this.threejs = new THREE.WebGLRenderer({
            antialias: true,
        })
        this.threejs.shadowMap.enabled = true
        this.threejs.shadowMap.type = THREE.PCFSoftShadowMap
        this.threejs.setPixelRatio(window.devicePixelRatio)
        this.threejs.setSize(window.innerWidth, window.innerHeight)
        document.body.appendChild(this.threejs.domElement)
    }
    thirdPersonCamSetup(fov = 60, aspect = window.innerWidth / window.innerHeight, near = 0.1, far = 2000, position = new THREE.Vector3(0, 0 , 0), lookAt= new THREE.Vector3(0,0,0)) {
        //{x: 25, y: 47, z: -44}
        let posCam = new THREE.Vector3(25+position.x, 47+position.y , -44+position.z)
        let cam = new THREE.PerspectiveCamera(fov, aspect, near, far)
        cam.position.set(posCam.x, posCam.y, posCam.z)
        cam.lookAt(lookAt)
        this.camera = cam
        // this.camControls = new FPSCamController(this.camera, this.comps.degToRad)
    } 
    skyboxSetup(index = '01') {
        const loader = new THREE.CubeTextureLoader()
        const texture = loader.load([
            'resources/skybox/'+index+'/posx.jpg',
            'resources/skybox/'+index+'/negx.jpg',
            'resources/skybox/'+index+'/posy.jpg',
            'resources/skybox/'+index+'/negy.jpg',
            'resources/skybox/'+index+'/posz.jpg',
            'resources/skybox/'+index+'/negz.jpg',
        ])
        this.scene.background = texture
    }
    initSubs(it){
        this.lights = new Lights(THREE, this)
        this.comps = new components()
        this.gui = new gui()
        this.inputCollector = new InputCollecter(it)
        this.lines = new Line()
    }
    initLoaders(){
        this.FBX = new FBX(THREE, this, CharacterController)
    }
    OnWindowResize() {
        try {
            this.camera.aspect = window.innerWidth / window.innerHeight
            this.camera.updateProjectionMatrix()
            this.threejs.setSize(window.innerWidth, window.innerHeight)
            // this.camControls.setScreen(window.innerWidth, window.innerHeight)
            // this.gui.elements.screen.innerText='width:'+window.innerWidth+ ' height:'+window.innerHeight
        }
        catch (err) {
            if (this.debug) {
                console.log("window resize fail")
                console.log(err)
            }
        }
    }
    RAF(){
        requestAnimationFrame((t) => {
            this.keys = this.inputCollector.keys
            this.cursor = this.inputCollector.cursor

            this.gui.elements.mouseX.innerText = 'x: '+this.cursor.x
            this.gui.elements.mouseY.innerText = 'y: '+this.cursor.y

            this.Step(t-this.prevRAF)
            this.prevRAF = t

            this.threejs.render(this.scene, this.camera)

            if(this.controller){
                this.controller.Update(t, this.inputCollector)
            }

            if(this.keys.displayEntities){
                console.log(this.entities)
            }
            // this.camControls.update(this.cursor)
            //this.camera.rotation.x += 0.01
            this.RAF()
        })
    }
    Step(timeElapsed) {
        const timeElapsedS = timeElapsed * 0.001
        if (this.mixers) {
          this.mixers.map(m => m.update(timeElapsedS))
        }
    }
    // IC(){
    //     this.fixedUpdate(this)
    // }
}

export {GAME}