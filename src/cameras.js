class ThirdPersonCamera{
    constructor(camera, THREE, mesh) {
        this.camera = camera
        this.THREE = THREE
        this.currPos = new THREE.Vector3()
        this.currLookAt = new THREE.Vector3()
        this.target = mesh
    }
    calculateIdealOffset(){
        const idealOffset = new this.THREE.Vector3(-15, 20, -30)
        idealOffset.applyQuaternion(this.target.quaternion)
        idealOffset.add(this.target.position)
        return idealOffset
    }
    calculateIdealLookAt(){
        const idealLookAt = new this.THREE.Vector3(0, 10, 30)
        idealLookAt.applyQuaternion(this.target.quaternion)
        idealLookAt.add(this.target.position)
        return idealLookAt
    }
    Update(){
        const idealOffset = this.calculateIdealOffset()
        const idealLookAt = this.calculateIdealLookAt()

        const t = 0.05

        this.currPos.lerp(idealOffset, t)
        this.currLookAt.lerp(idealLookAt,t)
        this.camera.position.copy(this.currPos)
        this.camera.lookAt(this.currLookAt)
    }
}
export {ThirdPersonCamera}