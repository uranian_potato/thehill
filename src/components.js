class components{
    constructor(){}
    getRandString(length=16){
        //'1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'
        let key = '1234567890abcdef'
        let l = key.length
        let ind = length
        let outStr = ''
        while(ind>0){
            let rand = Math.floor(Math.random() * l)
            outStr += key[rand]
            ind -= 1
        }
        return outStr
    
    }
    degToRad(deg, angle=90){
        return deg*Math.PI/angle
    }
}

export {components}