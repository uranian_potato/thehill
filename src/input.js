class standard{
    constructor() {
        this.keys = {
            forward: false,
            backward: false,
            left: false,
            right: false,
            space: false,
            shift: false,
            displayEntities: false,
            rotLeft: false,
            rotRight: false
        }
        this.mouse = {
            x:0,
            y:0,
            clicked:false
        }
    }
    init(){
        document.addEventListener('keydown', (e)=>{
            let c = e.key
            if(c == 'w' || c == 'ArrowUp'){
                this.keys.forward = true
            }else if(c == 's' || c == 'ArrowDown'){
                this.keys.backward = true
            }else if(c == 'a' || c == 'ArrowLeft'){
                this.keys.left = true
            }else if(c == 'd' || c == 'ArrowRight'){
                this.keys.right = true
            }else if(c == 'Shift'){
                this.keys.shift = true   
            }else if(e.code == 'Space'){
                this.keys.space = true   
            }else if(c == 'f'){
                this.keys.displayEntities = true
            }else if(c == 'q'){
                this.keys.rotLeft = true
            }else if(c == 'e'){
                this.keys.rotRight = true
            }
        })
        document.addEventListener('keyup', (e)=>{
            let c = e.key
            if(c == 'w' || c == 'ArrowUp'){
                this.keys.forward = false
            }else if(c == 's' || c == 'ArrowDown'){
                this.keys.backward = false
            }else if(c == 'a' || c == 'ArrowLeft'){
                this.keys.left = false
            }else if(c == 'd' || c == 'ArrowRight'){
                this.keys.right = false
            }else if(c == 'Shift'){
                this.keys.shift = false  
            }else if(e.code == 'Space'){
                this.keys.space = false 
            }else if(c == 'f'){
                this.keys.displayEntities = false
            }else if(c == 'q'){
                this.keys.rotLeft = false
            }else if(c == 'e'){
                this.keys.rotRight = false
            }
        })
        document.addEventListener('mousemove', (e)=>{
            this.mouse.x = e.x
            this.mouse.y = e.y
        })
        document.addEventListener('mousedown', (e)=>{
            this.mouse.clicked = true
        })
        document.addEventListener('mouseup', (e)=>{
            this.mouse.clicked = false
        })
    }
}
class InputCollecter{
    constructor(type='st'){
        this.types = {'st': standard}
        this.collector = new this.types[type]()
        this.collector.init()
    }
    get keys(){
        return this.collector.keys
    }
    get cursor(){
        return this.collector.mouse
    }
}

export {InputCollecter}