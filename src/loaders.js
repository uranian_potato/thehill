import {FBXLoader} from 'https://cdn.jsdelivr.net/npm/three@0.118.1/examples/jsm/loaders/FBXLoader.js'
class FBX {
    constructor(THREE, parent, charController){
        this.charController = charController
        this.THREE = THREE
        this.parent = parent
    }
    loadStaticFBX(path, type){
        try{
            let loader = new FBXLoader()
            loader.load(path, (fbx) => {
                fbx.scale.setScalar(0.1)
                fbx.traverse(c => {
                  c.castShadow = true
                })    
                if(this.parent.entities[type]){
                    this.parent.entities[type].push(fbx)
                    this.parent.scene.add(fbx)
                }
                else{
                    throw new Error('entity type: '+ type +' not supported')
                }
            })
        }
        catch(err){
            if(this.parent.debug){
                console.log(err)
            }
        }
    }
    loadAnimatedFBX(
        type='player',
        name=''+this.parent.entityCount,
        modelPath, 
        animPaths={}, camera=undefined)
        {
            let loader = new FBXLoader()
            loader.load(modelPath, (fbx) => {
                fbx.scale.setScalar(0.1)
                fbx.traverse(c => {
                    c.castShadow = true
                })
                try {

                    let animClips = {}
                    let mixer = new this.THREE.AnimationMixer(fbx)
                    this.parent.mixers.push(mixer)
                    
                    let index = 0

                    for(let key in animPaths){
                        let animload = new FBXLoader()
                        animload.load(animPaths[key], (anim) => {
                            try{
                                let animation = mixer.clipAction(anim.animations[0])
                                animClips[key] = animation
                                index++
                                if(index == Object.keys(animPaths).length){
                                    afterAnimLoad(this.parent, animClips, this.charController, this.THREE)
                                }
                            }
                            catch(err){
                                if(this.parent.debug){
                                    console.log(err)
                                }
                            }
                            
                        })
                    }
                
                }
                catch(err){
                    if(this.parent.debug){
                        console.log(err)
                    }
                }
                function afterAnimLoad(mega, animClips, cCon, three){
                    if(mega.entities[type]){
                        mega.entities[type] = {"name": name, "mesh": fbx, "animClips": animClips}
                        mega.scene.add(fbx)
                        if(type == 'player'){
                            let cam = new camera(mega.camera, three, mega.entities.player.mesh)
                            mega.controller = new cCon(mega, three, mega.lines, cam)
                            
                        } else if(type == 'objects'){
    
                        }
                        mega.entityCount++
                    }
                }
            })
        }
        
    }
export {FBX}