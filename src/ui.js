class gui{
    constructor() {
        this.elements = {}
        this.css = document.createElement('style')
        this.css.innerHTML = ''
    }
    addNewEll(id, attribs={},type='p'){
        const ell = document.createElement(type)
        ell.setAttribute('id', id)
        let styles=''
        for(let attrib in attribs){
            styles+= ''+attrib+':'+attribs[attrib]+';'
        }
        ell.setAttribute('style', styles)
        this.elements[id] = ell
        document.body.appendChild(ell)
    }
    addXandY(){
        this.addNewEll('mouseX', {
            'background-color': '#fff',
            'color':'#000',
            'position': 'absolute',
            'top': '40px',
            'padding-right':'40px'
        }, 'p')
        this.addNewEll('mouseY', {
            'background-color': '#fff',
            'color':'#000',
            'position': 'absolute',
            'top': '80px',
            'padding-right':'40px'
        }, 'p')
    }
    addCSS(ell='*', attribs={}){
        let styles='{'
        for(let attrib in attribs){
            styles+= ''+attrib+':'+attribs[attrib]+';'
        }
        styles+='}'
        this.css.innerHTML+=ell+styles
        document.head.appendChild(this.css)
    }
    addBasicP(id, top, left){
        this.addNewEll(id, {'position': 'absolute', 'top': top+'px', 'left':left+'px', 'background-color':'#ffff', 'color': 'rgb(0,0,0)'}, 'p')
    }
}

export {gui}