class Line{
    constructor() {
    }
    drawLines(THREE, scene, vecs){
        const material = new THREE.LineBasicMaterial( { color: 0x0000ff } )
        const points = vecs || [ 
            new THREE.Vector3( - 10, 0, 0),
            new THREE.Vector3( 0, 10, 0 ),
            new THREE.Vector3( 10, 0, 0 ) ]
        const geometry = new THREE.BufferGeometry().setFromPoints( points )
        const line = new THREE.Line( geometry, material );
        scene.add(line)
    }
}
export {Line}