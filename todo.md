# TODO

## GamePlay
- jumping and falling
    - deadline: NOV5
- collision detection
    - deadline: NOV6
- variable storage
    - deadline: NOV8
---
## Assets
- re-usable skybox
    - deadline: NOV3
- basic rigged model, fbx
    - deadline: NOV7
    - no skin
- art pack design (something newyork 1920's with an agrarian feel)
    - deadline: NOV20
---
## QA 
- docs
    - deadline: rolling
- wiki
    - deadline: rolling